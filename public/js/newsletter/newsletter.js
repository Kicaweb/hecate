/* global window, ouibounce, document, $, jQuery, location */

const findIP = new Promise((r) => {
  const w = window;
  const a = new (w.RTCPeerConnection || w.mozRTCPeerConnection || w.webkitRTCPeerConnection)({ iceServers: [] });
  const b = () => {};
  a.createDataChannel('');
  a.createOffer((c) => {
    return a.setLocalDescription(c, b, b);
  }, b);
  a.onicecandidate = (c) => {
    try {
      c.candidate.candidate.match(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g).forEach(r);
    } catch (e) {
      console.log(e);
    }
  };
});

ouibounce(document.getElementById('newsletter-modal'), {
  aggressive: false,
  sitewide: true,
});

jQuery('#close-button').on('click', () => {
  $('#newsletter-modal').hide();
});

$('.focus-text').focus(() => {
  $(this)
    .closest('div')
    .addClass('focus-t');
});
$('.focus-text').blur(() => {
  if ($('.focus-text').length > 0 && $('.focus-text').val() !== '') {
    $(this)
      .closest('div')
      .addClass('focus-t');
  } else {
    $(this)
      .closest('div')
      .removeClass('focus-t');
  }
});

$('#btnSubscribe').click(async (event) => {
  event.preventDefault();
  if (document.getElementById('newsletterForm').checkValidity() === false) {
    event.stopPropagation();
  } else {
    const formData = {
      name: $('input[name=name]').val(),
      email: $('input[name=email]').val(),
      urlOrigin: $(location).attr('href'),
      IP: await findIP,
    };

    $.ajax({
      type: 'POST',
      url: '/insertUser',
      data: formData,
      dataType: 'json',
      encode: true,
    }).done((data) => {
      $('#infoSubmit').fadeIn();
      if (data.result === false) {
        $('#errorSubmit').fadeIn();
        $('#successSubmit').fadeOut();
      } else {
        $('#errorSubmit').fadeOut();
        $('#successSubmit').fadeIn();
        $('#newsletterForm').fadeOut();
      }
    });
  }
  document.getElementById('newsletterForm').classList.add('was-validated');
});
