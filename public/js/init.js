/* global window, jQuery, document, $ */

$(document).ready(() => {
  $('[data-toggle="tooltip"]').tooltip();

  if ($('#back-to-top').length) {
    const scrollTrigger = 100; // px
    const backToTop = () => {
      const scrollTop = $(window).scrollTop();
      if (scrollTop > scrollTrigger) {
        $('#back-to-top').addClass('show');
      } else {
        $('#back-to-top').removeClass('show');
      }
    };
    backToTop();
    $(window).on('scroll', () => {
      backToTop();
    });
    $('#back-to-top').on('click', (e) => {
      e.preventDefault();
      $('html,body').animate(
        {
          scrollTop: 0,
        },
        700,
      );
    });
  }

  jQuery('.scroll_to').click((e) => {
    const jump = $(this).attr('href');
    const newPosition = $(jump).offset();
    $('html, body')
      .stop()
      .animate({ scrollTop: newPosition.top }, 500);
    e.preventDefault();
  });

  function setCookieEu(cName, value, exdays) {
    const exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    const cVame = escape(value) + (exdays == null ? '' : `; expires=${exdate.toUTCString()}`);
    document.cookie = `${cName}=${cVame}; path=/`;

    $('#cookie_directive_container').hide('slow');
  }

  function getCookieEu(cName) {
    let i;
    let x;
    let y;
    const ARRcookies = document.cookie.split(';');
    for (i = 0; i < ARRcookies.length; i += 1) {
      x = ARRcookies[i].substr(0, ARRcookies[i].indexOf('='));
      y = ARRcookies[i].substr(ARRcookies[i].indexOf('=') + 1);
      x = x.replace(/^\s+|\s+$/g, '');
      if (x === cName) {
        return unescape(y);
      }
    }
    return null;
  }

  function checkCookieEu() {
    const consent = getCookieEu('cookies_consent');

    if (consent == null || consent == '' || consent == undefined) {
      // show notification bar
      $('#cookie_directive_container').show();
    }
  }

  checkCookieEu();

  $('#accept').click(() => {
    setCookieEu('cookies_consent', 1, 30);
  });
});
