/* eslint-disable no-param-reassign */
/* global CONFIG */

require('dotenv').config();
global.CONFIG = process.env;

const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const dbUrl = `mongodb://${CONFIG.DB_USER}:${CONFIG.DB_PASSWORD}@${CONFIG.DB_HOSTNAME}:${CONFIG.DB_PORT}/${
  CONFIG.DB_NAME
}`;
mongoose.connect(dbUrl, { useNewUrlParser: true }, (err) => {
  if (err) throw err;
  console.log('[BOOT UP] Category Tool: Database connection OK');
});

const Scholarship = require('../server/models/scholarship');
const Category = require('../server/models/category');

/**
 * Website
 */
const webSite = 'en_so';

/**
 * Returns the number of scholarships which are relevant given a searchTerm and a webCode
 *
 * @param {*} webCode The website
 * @param {*} searchTerm The search term
 * @example
 * countScholarships(webSite, 'Tunisia');
 */
const countScholarships = async (webCode, searchTerm) => {
  console.log('--------------------------');
  console.log(`Start countScholarships with ${searchTerm}.`);
  console.log('--------------------------');
  const scholarships = await Scholarship.find(
    {
      $text: { $search: searchTerm },
      webData: {
        $elemMatch: {
          webCode,
        },
      },
    },
    { slug: 1, webData: 1 },
  );
  console.log(`Found ${scholarships.length} scholarships related to ${searchTerm}`);
  console.log(`For example: ${scholarships[0].slug}`);
  console.log('--------------------------');
  console.log(`End ${searchTerm}.`);
  console.log('--------------------------');
};

/**
 * Only categorizes the scholarship in one website and only if that category doesn't exists
 * in that scholarship
 *
 * @param {*} webCode The website
 * @param {*} category The name of the category and also the search term
 * @param {*} slug Category slug
 * @param {*} slug The search term to perform the query. If not available, searchTerm = category name
 * @example
 * categorize(webSite, 'Oceania', 'oceania', 'Australia');
 * categorize(webSite, 'America', 'america');
 */
const categorize = async (webCode, category, slug, searchTerm = null) => {
  console.log('--------------------------');
  console.log(`Start categorize with ${category}.`);
  console.log('--------------------------');
  if (!searchTerm) {
    // eslint-disable-next-line no-param-reassign
    searchTerm = category;
  }
  const scholarships = await Scholarship.find(
    {
      $text: { $search: searchTerm },
      webData: {
        $elemMatch: {
          webCode,
        },
      },
    },
    { slug: 1, webData: 1 },
  );
  const newCategory = { slug, name: category };
  let modify;

  await Promise.all(
    scholarships.map(async (scholarship) => {
      modify = false;

      await Promise.all(
        scholarship.webData.map(async (webData) => {
          if (webData.webCode === webCode) {
            // Website exists
            modify = true;
            await Promise.all(
              webData.categories.map((cat) => {
                if (cat.name === category) {
                  // Category exists in that website, so we don't insert it again
                  modify = false;
                }
                return true;
              }),
            );
            webData.categories.push(newCategory);
          }
        }),
      );

      if (modify) {
        Scholarship.findOneAndUpdate({ slug: scholarship.slug }, { $set: { webData: scholarship.webData } }, (err) => {
          if (err) {
            console.log('error', err);
          }
          console.log(`Scholarship modified: ${scholarship.slug}`);
        });
      }
    }),
  );

  console.log('--------------------------');
  console.log(`End ${category}.`);
  console.log('--------------------------');
};

/**
 * Sets a default category for all the scholarships without a category given a website
 *
 * @param {*} webCode The website
 * @param {*} category The name of the default category
 * @param {*} slug The slug of the default category
 * @example
 * atLeastOneCategory(webSite, 'International', 'international');
 */
const atLeastOneCategory = async (webCode, category, slug) => {
  console.log('--------------------------');
  console.log(`Start atLeastOneCategory with ${category}.`);
  console.log('--------------------------');
  const scholarships = await Scholarship.find({ webData: { $elemMatch: { webCode } } });
  const newCategory = { slug, name: category };

  await Promise.all(
    scholarships.map(async (scholarship) => {
      await Promise.all(
        scholarship.webData.map((webData) => {
          if (webData.webCode === webCode) {
            // Website exists
            if (webData.categories.length < 1) {
              // And that scholarship hasn't any categories
              webData.categories.push(newCategory);
              Scholarship.findOneAndUpdate(
                { slug: scholarship.slug },
                { $set: { webData: scholarship.webData } },
                (err) => {
                  if (err) {
                    console.log('error', err);
                  }
                  console.log(`Scholarship modified: ${scholarship.slug}`);
                },
              );
              console.log('This scholarship has no categories', scholarship);
            }
          }
          return true;
        }),
      );
    }),
  );

  console.log('--------------------------');
  console.log(`End ${category}.`);
  console.log('--------------------------');
};

/**
 * Sets the name of the scholarship as the title in the webData section
 *
 * @param {*} webCode The website
 * @example
 * repairTitleWebData(webSite);
 */
const repairTitleWebData = async (webCode) => {
  console.log('--------------------------');
  console.log(`Start repairTitleWebData ${webCode}.`);
  console.log('--------------------------');
  const scholarships = await Scholarship.find({ webData: { $elemMatch: { webCode } } });

  await Promise.all(
    scholarships.map(async (scholarship) => {
      if (scholarship.webData.length === 1) {
        scholarship.webData[0].title = scholarship.title;
        await Scholarship.findOneAndUpdate(
          { slug: scholarship.slug },
          { $set: { webData: scholarship.webData } },
          (err) => {
            if (err) {
              console.log('error', err);
            }
            console.log(`Scholarship modified: ${scholarship.slug}`);
          },
        );
      }
    }),
  );

  console.log('--------------------------');
  console.log(`End repairing ${webCode}.`);
  console.log('--------------------------');
};

/**
 * Sets the scholarshipCount param to all the categories of a website
 *
 * @param {*} webCode The website
 * @example
 * setScholarshipCount(webSite);
 */
const setScholarshipCount = async (webCode) => {
  console.log('--------------------------');
  console.log(`Start setScholarshipCount ${webCode}.`);
  console.log('--------------------------');

  const categories = await Category.find({ webCode, subCategories: { $size: 0 }, parent: { $exists: true } });

  await Promise.all(
    categories.map(async (category) => {
      const number = await Scholarship.countDocuments({
        webData: {
          $elemMatch: {
            webCode,
            publishDate: {
              $lte: new Date(),
            },
            categories: {
              $elemMatch: { slug: category.slug },
            },
          },
        },
      });
      console.log(`There are ${number} scholarships for ${category.slug}`);
      Category.findOneAndUpdate({ slug: category.slug, webCode }, { $set: { scholarshipCount: number } }, (err) => {
        if (err) {
          console.log('error', err);
        }
        console.log(`Category modified: ${category.slug}`);
      });
    }),
  );

  console.log('--------------------------');
  console.log(`ScholarshipCount added to the categories of ${webCode}.`);
  console.log('--------------------------');
};

/**
 * Adds children categories to database
 *
 * @param {*} webCode The website
 * @param {*} parentSlug The slug of the parent Category
 * @param {*} parentName The name of the parent Category
 * @param {*} categorySlug The slug of the Category
 * @param {*} categoryNamecategoryName The name of the Category
 * @example
 * addChildrenCategory(webSite, 'europe', 'Europe', 'spain', 'Spain');
 */
const addChildrenCategory = async (webCode, parentSlug, parentName, categorySlug, categoryName) => {
  console.log('--------------------------');
  console.log(`Start addChildrenCategory ${webCode}.`);
  console.log('--------------------------');

  const categoryJson = JSON.parse(
    `{"parent" : {"slug" : "${parentSlug}", "name" : "${parentName}"}, "webCode" : "${webSite}",
      "name" : "${categoryName}", "slug" : "${categorySlug}"}`,
  );

  const catDb = new Category(categoryJson);
  catDb.save((err) => {
    if (err) console.log(`Error Saving category ${catDb}`);
    console.log(`Category ${categoryName} saved`);
  });

  console.log('--------------------------');
  console.log(`addChildrenCategory finished on ${webCode}.`);
  console.log('--------------------------');
};

/**
 * Adds parent categories to database. This method will search for the other categories of the website,
 * getting the parent name and slug and creating a new category.
 *
 * @param {*} webCode The website
 * @param {*} parentSlug The slug of the parent Category
 * @param {*} parentName The name of the parent Category
 * @example
 * addParentCategory(webSite, 'studies', 'Studies');
 */
const addParentCategory = async (webCode, parentSlug, parentName) => {
  console.log('--------------------------');
  console.log(`Start addParentCategory ${webCode}.`);
  console.log('--------------------------');

  const categoryJson = JSON.parse(
    `{"name" : "${parentName}", "slug" : "${parentSlug}", "webCode" : "${webSite}", "subCategories" : []}`,
  );

  const categories = await Category.find({ webCode, 'parent.slug': parentSlug });

  await Promise.all(
    categories.map(async (category) => {
      categoryJson.subCategories.push(JSON.parse(`{"slug" : "${category.slug}", "name" : "${category.name}"}`));
    }),
  );

  const catDb = new Category(categoryJson);
  catDb.save((err) => {
    if (err) console.log(`Error Saving category ${catDb}`);
    console.log(`Category ${parentName} saved`);
  });

  console.log('--------------------------');
  console.log(`addParentCategory ${parentName} finished on ${webCode}.`);
  console.log('--------------------------');
};
