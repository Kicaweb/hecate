/* global CONFIG */

const hbs = require('hbs');
const ads = require('../../assets/ads.json');

hbs.registerHelper('insertAd', (id) => {
  const adSelected = ads.find((x) => {
    return x.ID === id;
  });

  let type = 'fixed';

  if (adSelected.responsive) {
    type = 'responsive';
  }

  let format = adSelected.data_ad_format;

  if (adSelected.data_ad_format == null) {
    format = 'rectangle';
  }

  let ad = `<div class="${adSelected.class_container} ${format}-${type}">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                    style="${adSelected.style}"
                    data-ad-client="${CONFIG.GOOGLE_ADSENSE_ID}"
                    data-ad-slot="${adSelected.data_ad_slot}"`;

  if (adSelected.data_ad_format) ad += ` data-ad-format="${adSelected.data_ad_format}"`;
  if (adSelected.responsive) ad += ' data-full-width-responsive="true"';

  ad += `">
    </ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script></div>`;
  return ad;
});
