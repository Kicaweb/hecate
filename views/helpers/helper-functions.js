/* global CONFIG */

const hbs = require('hbs');
const moment = require('moment');
const striptags = require('striptags');

const i18n = require('i18n');

moment.locale(CONFIG.MOMENT_LOCALE);

hbs.registerHelper('categoryTitle', (category) => {
  let scholarshipWord = i18n.__('scholarship.pluralWord');
  switch (CONFIG.language) {
    case 'en':
      return `${category.name} ${scholarshipWord} in ${CONFIG.YEAR}`;
    case 'hi':
      return `${category.name} ${scholarshipWord} ${CONFIG.YEAR}`;
    default:
      scholarshipWord = scholarshipWord.charAt(0).toUpperCase() + scholarshipWord.slice(1);
      return `${scholarshipWord} ${category.name} en ${CONFIG.YEAR}`;
  }
});

hbs.registerHelper('addAmpToLinks', (text) => {
  return text.replace(new RegExp('href=["\'](.*?)["\']', 'ig'), "href='$1/amp/'");
});

hbs.registerHelper('categoryMainText', (name) => {
  const text = i18n.__('category.descriptionText');
  return text.replace(new RegExp('\\[CATEGORY\\]', 'ig'), name);
});

hbs.registerHelper('config', (configVar) => {
  return CONFIG[configVar];
});

const DateFormats = {
  day: 'DD',
  month: 'MMMM',
  dayMonth: 'DD - MMMM',
  long: 'DD/MM/YYYY',
  abr: 'll',
  extended: 'LL',
};

// register hbs helpers in res.locals' context which provides this.locale
hbs.registerHelper('__', function() {
  return i18n.__.apply(this, arguments);
});
hbs.registerHelper('__n', function() {
  return i18n.__n.apply(this, arguments);
});

hbs.registerHelper('dateFormatScholarship', (datetime, start, format) => {
  try {
    if (false) {
      const m = moment.utc(dateend);
      const { today } = moment;
      const isafter = m.isBefore(today);
      if (isafter) {
        return i18n.__('scholarship.waiting');
      }
    }
    if (datetime && format) {
      if (moment) {
        const newFormat = DateFormats[format] || DateFormats.long;
        return moment.utc(datetime).format(newFormat);
      }
      return datetime;
    }
    return i18n.__('scholarship.waiting');
  } catch (error) {
    console.log(error);
    return datetime;
  }
});

hbs.registerHelper('dateFormat', (datetime, format, checkPass, dateend) => {
  try {
    if (checkPass) {
      const m = moment.utc(dateend);
      const { today } = moment;
      const isafter = m.isBefore(today);
      if (isafter) {
        return i18n.__('scholarship.waiting');
      }
    }
    if (datetime && format) {
      if (moment) {
        const newFormat = DateFormats[format] || DateFormats.long;
        return moment.utc(datetime).format(newFormat);
      }
      return datetime;
    }
    return i18n.__('scholarship.waiting');
  } catch (error) {
    console.log(error);
    return datetime;
  }
});

hbs.registerHelper('formatCategories', (categories) => {
  try {
    const categoriesAux = [];
    categories.forEach((category) => {
      categoriesAux.push(category.name);
    });
    return categoriesAux.join(', ');
  } catch (error) {
    console.log(error);
    return categories;
  }
});

hbs.registerHelper('prevnext', (paginate, pages, pageCount) => {
  let headPrevNext = '';
  // eslint-disable-next-line no-param-reassign
  if (!pageCount) pageCount = 0;
  if (paginate.hasPreviousPages || paginate.hasNextPages(pageCount)) {
    if (paginate.hasPreviousPages) {
      headPrevNext += `<link rel="prev" href="${paginate.href(true, {})}" />`;
    }
    if (paginate.hasNextPages(pageCount)) {
      headPrevNext += `<link rel="next" href="${paginate.href()}" />`;
    }
  }
  return headPrevNext;
});

hbs.registerHelper('firstUpp', (texto) => {
  try {
    return texto.charAt(0).toUpperCase() + texto.slice(1);
  } catch (error) {
    console.log(error);
    return texto;
  }
});

hbs.registerHelper('primerParrafo', (texto) => {
  try {
    const parrafos = texto.split('<p>');

    if (parrafos[1] === undefined) {
      return `<p>${texto}</p>`;
    }
    return `<p>${parrafos[1]}`;
  } catch (error) {
    console.log(error);
    return texto;
  }
});

const subStrCompose = (string, field, badgeText) => {
  try {
    const cleanField = striptags(field);

    let regExpCadena = string.replace(new RegExp('(a|á)', 'ig'), '(a|á)');
    regExpCadena = regExpCadena.replace(new RegExp('(e|é)', 'ig'), '(e|é)');
    regExpCadena = regExpCadena.replace(new RegExp('(i|í)', 'ig'), '(i|í)');
    regExpCadena = regExpCadena.replace(new RegExp('(o|ó)', 'ig'), '(o|ó)');
    regExpCadena = regExpCadena.replace(new RegExp('(u|ú)', 'ig'), '(u|ú)');

    const reg = new RegExp(regExpCadena, 'ig');

    const index = cleanField.search(reg);

    let composedString = '';

    if (index > -1) {
      let firstChar = -300;
      let lastChar = 300;

      if (index < 300) {
        firstChar = 0;
        lastChar = 600 - index;
      } else {
        firstChar = index - 300;
        lastChar = firstChar + 600;
      }

      if (firstChar > 0) {
        composedString += `<p><span class="badge badge-info">${badgeText}</span> [...]`;
      } else {
        composedString += `<p><span class="badge badge-info">${badgeText}</span> `;
      }

      composedString += cleanField.substring(firstChar, lastChar);

      if (lastChar < cleanField.length) {
        composedString += '...</p>';
      } else {
        composedString += '</p>';
      }

      composedString = composedString.replace(string, `<b>${string}</b>`);

      return composedString;
    }
    const words = string.split(' ');

    if (words.length > 1) {
      let addText = true;

      words.forEach((word) => {
        if (word.length > 3) {
          let regExpWord = word.replace(new RegExp('a', 'ig'), '(a|á)');
          regExpWord = regExpWord.replace(new RegExp('e', 'ig'), '(e|é)');
          regExpWord = regExpWord.replace(new RegExp('i', 'ig'), '(i|í)');
          regExpWord = regExpWord.replace(new RegExp('o', 'ig'), '(o|ó)');
          regExpWord = regExpWord.replace(new RegExp('u', 'ig'), '(u|ú)');
          const regWord = new RegExp(regExpWord, 'ig');

          const indexWord = cleanField.search(regWord);

          if (indexWord > -1) {
            let firstChar = -300;
            let lastChar = 300;
            if (indexWord < 300) {
              firstChar = 0;
              lastChar = 600 - indexWord;
            } else {
              firstChar = indexWord - 300;
              lastChar = firstChar + 600;
            }

            if (addText) {
              if (firstChar > 0) {
                composedString += `<p><span class="badge badge-info">${badgeText}</span> [...]`;
              } else {
                composedString += `<p><span class="badge badge-info">${badgeText}</span> `;
              }

              composedString += cleanField.substring(firstChar, lastChar);

              if (lastChar < cleanField.length) {
                composedString += '...</p>';
              } else {
                composedString += '</p>';
              }
            }
            composedString = composedString.replace(regWord, `<b>${word}</b>`);
            addText = false;
          }
        }
      });
    }
    return composedString;
  } catch (error) {
    console.log(error);
    return string;
  }
};

hbs.registerHelper('resultCompose', (scholarship, cadena) => {
  const { description } = scholarship;
  let cadenaCompuesta = '';

  const userQuery = cadena.replace(new RegExp('class', 'ig'), '');

  if (description !== undefined) {
    const descriptionComposer = subStrCompose(userQuery, description, i18n.__('searchEngine.info'));
    cadenaCompuesta += descriptionComposer;
  }

  return cadenaCompuesta;
});

hbs.registerHelper('replaceStr', (text, string, replaceFor) => {
  return text.replace(string, replaceFor);
});

hbs.registerHelper('each_upto', (ary, min, max, options) => {
  try {
    if (!ary || ary.length === 0) return options.inverse(this);

    const result = [];
    // eslint-disable-next-line no-plusplus
    for (let i = min; i < max && i < ary.length; ++i) result.push(options.fn(ary[i]));
    return result.join('');
  } catch (error) {
    console.log(error);
    return ary;
  }
});

hbs.registerHelper('each_catparent', (categories, parent, options) => {
  const result = [];
  if (!categories || categories.length === 0) {
    console.log('error');
  }

  categories.forEach((category) => {
    if (category.parent === parent) {
      result.push(options.fn(category));
    }
  });

  return result.join('');
});

hbs.registerHelper('has_expired', function(dateString, options) {
  if (moment().isAfter(moment(dateString))) {
    return options.fn(this);
  }
  return options.inverse(this);
});

// eslint-disable-next-line space-before-function-paren
hbs.registerHelper('ifCond', function(v1, operator, v2, options) {
  switch (operator) {
    case '==':
      // eslint-disable-next-line eqeqeq
      return v1 == v2 ? options.fn(this) : options.inverse(this);
    case '===':
      return v1 === v2 ? options.fn(this) : options.inverse(this);
    case '!=':
      // eslint-disable-next-line eqeqeq
      return v1 != v2 ? options.fn(this) : options.inverse(this);
    case '!==':
      return v1 !== v2 ? options.fn(this) : options.inverse(this);
    case '<':
      return v1 < v2 ? options.fn(this) : options.inverse(this);
    case '<=':
      return v1 <= v2 ? options.fn(this) : options.inverse(this);
    case '>':
      return v1 > v2 ? options.fn(this) : options.inverse(this);
    case '>=':
      return v1 >= v2 ? options.fn(this) : options.inverse(this);
    case '&&':
      return v1 && v2 ? options.fn(this) : options.inverse(this);
    case '||':
      return v1 || v2 ? options.fn(this) : options.inverse(this);
    default:
      return options.inverse(this);
  }
});

hbs.registerHelper('pagination', (paginate, pages, pageCount) => {
  let htmlPagination = '';

  if (paginate.hasPreviousPages || paginate.hasNextPages(pageCount)) {
    htmlPagination += `<div class="navigation well-sm float-right mr-2" id="pagination">
      <ul class="pagination">`;

    if (paginate.hasPreviousPages) {
      htmlPagination += `
          <li class="page-item">
              <a href="${paginate.href(true, {})}" class="page-link">
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">${CONFIG.PAGINATION_PREVIOUS_LABEL}</span>
              </a>
          </li>`;
    }
    if (pages) {
      pages.forEach((page) => {
        htmlPagination += '<li class="page-item';
        if (paginate.page === page.number) {
          htmlPagination += ' active';
        }
        htmlPagination += `"><a href="${page.url}" class="page-link">${page.number}</a></li>`;
      });
    }
    if (paginate.hasNextPages(pageCount)) {
      htmlPagination += `<li class="page-item"><a href="${paginate.href()}" class="page-link">
          <span aria-hidden="true">&raquo;</span>
      <span class="sr-only">${CONFIG.PAGINATION_NEXT_LABEL}</span></a></li>`;
    }
    htmlPagination += '</ul></div>';
  }
  return htmlPagination;
});

hbs.registerHelper('ogImage', (canonical) => {
  let imagePath = canonical;
  if (canonical) {
    if (canonical.split('/').length - 1 === 4) {
      // Scholarship og:image
      imagePath = canonical.replace(new RegExp(CONFIG.URL, 'ig'), '');
      imagePath = imagePath.slice(0, -1);
      imagePath = `${CONFIG.URL}image/${imagePath}.jpg`;
    } else {
      // Home/Category/Page og:image
      imagePath = `${CONFIG.URL}img/${CONFIG.HD_IMG_NAME}`;
    }
  }
  return imagePath;
});
