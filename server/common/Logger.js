/* global CONFIG */

const currentDate = require('./Helper').getCurrentDate();

class Logger {
  static async exception(tag, req, err) {
    const ip = req.headers['x-forwarded-for'];
    const url = req.originalUrl;
    console.error(`${await currentDate} [${ip}]-[${tag}] Url:${url} - Error: ${err.description}`);
  }

  static async warn(tag, err) {
    console.warn(`${await currentDate} [${CONFIG.WEB_TITLE}]-[${tag}] - Error: ${err.description}`);
  }

  static async log(tag, req, err) {
    const ip = req.headers['x-forwarded-for'];
    console.log(`${await currentDate} [${ip}]-[${tag}] ${err.description}`);
  }

  static async init() {
    console.log(`${await currentDate} [BOOT UP] ${CONFIG.WEB_TITLE}: (${CONFIG.URL}) running`);
  }

  static async dbConnection(status) {
    console.log(`${await currentDate} [BOOT UP] ${CONFIG.WEB_TITLE}: Database connection ${status}`);
  }
}

module.exports = Logger;
