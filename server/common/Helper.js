/* global CONFIG */

const Logger = require('./Logger');

class Helper {
  static async spin(spyntax, code) {
    let text = spyntax.trim();
    if (text === '') {
      Logger.log('Please input spyntax content!');
      return spyntax;
    }
    if (text.split('{').length !== text.split('}').length) {
      Logger.log('SPIN', null, 'Incorrect spyntax in content! Please re-check it!');
      return spyntax;
    }

    let match;

    let reg = new RegExp(/{([^{}]*)}/i);
    do {
      match = reg.exec(text);
      if (match) {
        let array = [];
        array = match[1].split('|');
        text = text.replace(match[0], array[Math.floor(code * array.length)]);
      }
    } while (match);

    reg = new RegExp(/\{\{([\s\S]*?)\}\}/i);
    do {
      match = reg.exec(text);
      if (match) {
        let array = [];
        array = match[1].split('||');
        text = text.replace(match[0], array[Math.floor(code * array.length)]);
      }
    } while (match);

    return text;
  }

  static async createUrlsSitemap(scholarships, parentCategories, childrenCategories) {
    const urlsSitemap = new Set();
    urlsSitemap.add({ url: '/', changefreq: 'weekly', priority: 1 });

    // Adding scholarships to sitemap
    scholarships.forEach((scholarship) => {
      urlsSitemap.add({
        url: `/${scholarship.slug}/`,
        changefreq: CONFIG.SM_CHANGE_SCHOLARSHIPS,
        priority: Number(CONFIG.SM_PRIORITY_SCHOLARSHIPS),
      });
    });

    // Adding parent categories to sitemap
    parentCategories.forEach((category) => {
      urlsSitemap.add({
        url: `/${CONFIG.CATEGORY_BASE}/${category.slug}/`,
        changefreq: CONFIG.SM_CHANGE_CATEGORIES,
        priority: Number(CONFIG.SM_PRIORITY_PARENT_CATEGORIES),
      });
    });

    // Adding children categories to sitemap
    childrenCategories.forEach((category) => {
      urlsSitemap.add({
        url: `/${CONFIG.CATEGORY_BASE}/${category.slug}/`,
        changefreq: CONFIG.SM_CHANGE_CATEGORIES,
        priority: Number(CONFIG.SM_PRIORITY_CHILDREN_CATEGORIES),
      });
    });

    return urlsSitemap;
  }

  static async getCurrentDate() {
    const currentdate = new Date();
    const day = currentdate.getDate();
    const month = currentdate.getMonth() + 1;
    const year = currentdate.getFullYear();
    const hour = currentdate.getHours();
    const minutes = currentdate.getMinutes();
    const seconds = currentdate.getSeconds();
    const datetime = `${day}/${month}/${year} ${hour}:${minutes}:${seconds}`;
    return datetime;
  }

  static async removeGetParamsFromUrl(url) {
    return url.substring(0, url.indexOf('?'));
  }

  static async preparePagination(scholarshipCount, paginate, req) {
    const pageCount = Math.ceil(scholarshipCount / CONFIG.PAGINATION_LIMIT);
    const pages = paginate.getArrayPages(req)(Number(CONFIG.MAX_PAGES_DISPLAYED), pageCount, req.query.page);
    return [pageCount, pages];
  }

  static async prepareError(name, description, number, params = {}) {
    const error = new Error();
    error.name = name;
    if (description) error.description = description;
    error.number = number;
    if (params.redirect) error.redirect = params.redirect;
    throw error;
  }
}

module.exports = Helper;
