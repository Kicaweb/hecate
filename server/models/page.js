const mongoose = require('mongoose');
const { Schema } = mongoose;

const pageSchema = new Schema({
  name: { type: String, required: true },
  webCode: { type: String, required: true },
  slug: { type: String, required: true },
  active: { type: Boolean, default: true },
  title: { type: String, required: true },
  content: { type: String, required: true },
  seo: { metaTitle: String, metaDescription: String, noIndex: { type: Boolean, default: false } },
  sections: Object,
  descriptions: [{ _id: false, title: String, description: String }],
  insertDate: { type: Date, default: Date.now },
  lastEditionDate: { type: Date, default: Date.now },
});

const Page = mongoose.model('Page', pageSchema, 'pages');

module.exports = Page;
