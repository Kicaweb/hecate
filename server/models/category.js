const mongoose = require('mongoose');
const { Schema } = mongoose;

const categorySchema = new Schema({
  name: { type: String, required: true },
  webCode: { type: String, required: true },
  slug: { type: String, required: true },
  scholarshipCount: { type: Number, default: 0 },
  active: { type: Boolean, default: true },
  default: { type: Boolean, default: true },
  featured: { type: Boolean, default: false },
  seo: { metaTitle: String, metaDescription: String, noIndex: { type: Boolean, default: false } },
  parent: { id: String, name: String, slug: String },
  subCategories: [
    {
      _id: false,
      id: String,
      name: String,
      slug: String,
      featured: { type: Boolean, default: false },
    },
  ],
  mainTitle: String,
  mainDescription: String,
  subTitle: String,
  subDescription: String,
  extraDescriptions: [{ _id: false, title: String, description: String }],
  insertDate: { type: Date, default: Date.now },
  lastEditionDate: { type: Date, default: Date.now },
});

const Category = mongoose.model('Category', categorySchema, 'categories');

module.exports = Category;
