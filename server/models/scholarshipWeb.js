class ScholarshipWeb {
  constructor(
    _id,
    source,
    sponsor,
    amount,
    endDuration,
    startDuration,
    contact,
    title,
    seo,
    tags,
    localizations,
    categories,
    slug,
    comments,
    commentsCount,
    description,
    benefits,
    requirements,
    featured,
    publishDate,
    lastEditionDate,
  ) {
    this._id = _id;
    this.source = source;
    this.sponsor = sponsor;
    this.amount = amount;
    this.endDuration = endDuration;
    this.startDuration = startDuration;
    this.contact = contact;
    this.title = title;
    this.seo = seo;
    this.tags = tags;
    this.localizations = localizations;
    this.categories = categories;
    this.slug = slug;
    this.comments = comments;
    this.commentsCount = commentsCount;
    this.description = description;
    this.benefits = benefits;
    this.requirements = requirements;
    this.featured = featured;
    this.publishDate = publishDate;
    this.lastEditionDate = lastEditionDate;
  }

  /**
   * Compose a scholarshipWeb Object with a scholarship Object
   * @param {*} scholarship Scholarship object to transform to scholarshipWeb
   */
  static prepare(scholarship) {
    if (!scholarship.duration) {
      scholarship.duration = {};
    }
    const scholarshipWeb = new ScholarshipWeb(
      scholarship._id,
      scholarship.source,
      scholarship.sponsor,
      scholarship.amount,
      scholarship.duration.end,
      scholarship.duration.start,
      scholarship.contact,
      scholarship.webData[0].title,
      scholarship.webData[0].seo,
      scholarship.webData[0].tags,
      scholarship.webData[0].localizations,
      scholarship.webData[0].categories,
      scholarship.webData[0].slug,
      scholarship.webData[0].comments,
      scholarship.webData[0].commentsCount,
      scholarship.webData[0].content.description,
      scholarship.webData[0].content.benefits,
      scholarship.webData[0].content.requirements,
      scholarship.webData[0].featured,
      scholarship.webData[0].publishDate,
      scholarship.webData[0].lastEditionDate,
    );
    return scholarshipWeb;
  }
}
module.exports = ScholarshipWeb;
