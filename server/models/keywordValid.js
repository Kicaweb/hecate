const mongoose = require('mongoose');
const { Schema } = mongoose;

const keywordValidSchema = new Schema({
  text: { type: String, required: true, unique: true },
  slug: { type: String, required: true },
  lastAccess: Date,
  landingSelected: String,
  webCode: { type: String, required: true },
});

const KeywordValid = mongoose.model('KeywordValid', keywordValidSchema);
module.exports = KeywordValid;
