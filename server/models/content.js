const mongoose = require('mongoose');
const { Schema } = mongoose;

const contentSchema = new Schema({
  title: { type: String },
  content: { type: String },
  webCode: { type: String },
  section: { type: String },
  page: { type: String },
});

const Content = mongoose.model('Content', contentSchema);
module.exports = Content;
