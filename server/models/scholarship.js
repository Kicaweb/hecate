const mongoose = require('mongoose');
const { Schema } = mongoose;

const commentStatusValues = {
  values: ['approved', 'pending', 'deleted'],
  message: '{VALUE} is not valid',
};

const languageValues = {
  values: ['spanish', 'english', 'french'],
  message: '{VALUE} is not valid',
};

const scholarshipSchema = new Schema({
  title: { type: String, unique: true, required: [true, 'Title is required'] },
  description: { type: String, required: true },
  duration: { start: { type: Date, default: undefined }, end: { type: Date, default: undefined } },
  benefits: { type: String },
  amount: { type: String },
  requirements: { type: String },
  source: { name: { type: String }, url: { type: String } },
  contact: {
    name: String,
    email: String,
    phone: String,
    address: String,
    coords: { lat: String, lng: String },
  },
  sponsor: { text: String, url: String },
  language: { type: String, enum: languageValues },
  insertDate: { type: Date, default: Date.now },
  lastEditionDate: { type: Date, default: Date.now },
  webData: [
    {
      _id: false,
      webCode: String,
      title: String,
      seo: {
        metaTitle: { type: String },
        metaDescription: { type: String },
        noIndex: { type: Boolean, default: false },
      },
      tags: [{ type: String }],
      localizations: { origin: [String], destination: [String] },
      categories: [
        {
          _id: false,
          id: String,
          name: String,
          slug: String,
        },
      ],
      slug: String,
      oldSlugs: [String],
      comments: [
        {
          _id: false,
          status: { type: String, default: 'approved', enum: commentStatusValues },
          name: String,
          email: String,
          message: String,
          date: { type: Date, default: Date.now },
        },
      ],
      commentsCount: { type: Number, default: 0 },
      content: {
        _id: false,
        description: { type: String, required: true },
        benefits: String,
        requirements: String,
      },
      featured: { type: Boolean, default: false },
      publishDate: { type: Date, default: Date.now },
      lastEditionDate: { type: Date, default: Date.now },
    },
  ],
});

const Scholarship = mongoose.model('Scholarship', scholarshipSchema, 'scholarships');
module.exports = Scholarship;
