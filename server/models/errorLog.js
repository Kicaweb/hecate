const mongoose = require('mongoose');
const { Schema } = mongoose;

const errorLogSchema = new Schema({
  webCode: { type: String, required: true },
  ip: { type: String },
  action: { type: String },
  section: { type: String },
  description: { type: String },
  url: { type: String },
  userAgent: { type: String },
  bot: { type: String },
  stack: { type: String },
  responseCode: { type: Number },
  dateTime: { type: Date, default: Date.now },
});

const ErrorLog = mongoose.model('ErrorLog', errorLogSchema, 'error_log');

module.exports = ErrorLog;
