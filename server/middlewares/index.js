/* global CONFIG */
const express = require('express');
const app = express();

const _ = require('lodash');
const bodyParser = require('body-parser');
const cache = require('cache-control');
const minifyHTML = require('express-minify-html');
const minify = require('express-minify');
const favicon = require('express-favicon');
const compression = require('compression');
const i18n = require('i18n');

const Logger = require('../common/Logger');
const redirects = require('../../assets/redirects.json');
const forbidenWords = require('../../assets/forbiddenWords.json');

i18n.configure({
  locales: CONFIG.LANGUAGE,
  defaultLocale: CONFIG.LANGUAGE,
  objectNotation: true,
  directory: `${__dirname}/../../assets/languages`,
});

app.use(i18n.init);

app.enable('trust proxy');

app.use(async (request, response, next) => {
  try {
    let continueNext = false;

    _.map(forbidenWords, (word) => {
      if (request.url.includes(word)) {
        continueNext = true;
        Logger.warn('BLACKLIST', request, 'Keyword denied');
        response.status(400).send('No existe la página');
      }
    });
    let urlReturn = '';

    if (request.url.endsWith('amp')) {
      urlReturn = `${request.url}/`;
    }

    redirects.forEach((redirect) => {
      const redirectTo = Object.keys(redirect)[0];
      let routes = redirect[redirectTo];

      routes = routes.join('|');
      const regexUrl = new RegExp(`^/(${routes})/(.*?)/?(.*?)(/|\\.(.*?)$|$)`, 'g');

      const result = regexUrl.exec(request.url);

      if (result) {
        const param = result[3] + result[4];
        switch (redirectTo) {
          case 'CATALOG':
            urlReturn = `/${CONFIG.CATALOG_BASE}/${param}`;
            break;
          case 'CATEGORY':
            urlReturn = `/${CONFIG.CATEGORY_BASE}/${param}`;
            break;
          case 'HOME':
            urlReturn = '/';
            break;
          case 'IMAGE':
            urlReturn = `/catalog-image/${param}`;
            break;
          case 'IMAGE-ARTICLE':
            urlReturn = `/image/${param}`;
            break;
          default:
            urlReturn = `/${redirectTo}/`;
            break;
        }
      }
    });

    if (urlReturn !== '') {
      Logger.warn('REDIRECT', { description: `Redirect executed from ${request.url} to ${urlReturn}` });
      response.redirect(301, urlReturn);
      continueNext = true;
    }
    if (!continueNext) {
      next();
    }
  } catch (error) {
    console.log('error.stack :', error.stack);
    Logger.exception('BLACKLIST', request, error);
  }
});

app.use(favicon(`${__dirname}/../../public/img/favicon.ico`));

app.use(compression());

app.use(minify());

app.use(
  minifyHTML({
    override: true,
    exception_url: [/^.*\/amp/, /^robots\.txt/],
    htmlMinifier: {
      removeComments: true,
      collapseWhitespace: true,
      collapseBooleanAttributes: true,
      removeAttributeQuotes: true,
      removeEmptyAttributes: true,
      minifyJS: true,
    },
  }),
);

app.use(
  cache({
    '/**/*.jpg': 'max-age=3600',
    '/**/*.jpeg': 'max-age=3600',
    '/**/*.png': 'max-age=3600',
    '/**/*.gif': 'max-age=3600',
    '/**/*.ico': 'max-age=3600',
    '/**/*.css': 'max-age=3600',
    '/**/*.js': 'max-age=3600',
    '/**': 'max-age=120',
  }),
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

module.exports = app;
