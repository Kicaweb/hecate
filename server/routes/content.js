/* global CONFIG */

const express = require('express');
const app = express();
const i18n = require('i18n');

const Category = require('../controllers/Category');
const Catalog = require('../controllers/Catalog');
const Page = require('../controllers/Page');
const Seo = require('../controllers/Seo');
const errorLogController = require('../controllers/ErrorLog');

/**
 * Homepage
 */
app.get(/^\/(amp\/)?$/, async (req, res) => {
  try {
    const page = await Page.getHomeContent();
    const [categories, keywords, metas] = await Promise.all([
      Category.getHighestParentCategories(),
      Catalog.getSidebarKeywords(),
      Seo.getMetas(CONFIG.SEO_TITLE, CONFIG.SEO_METADESCRIPTION, CONFIG.OG_WEBSITE, false, req, 'home'),
    ]);
    let urlToRender = 'home';
    if (req.params[0] === 'amp/') {
      urlToRender = 'amp/home';
    }

    res.render(urlToRender, {
      page,
      categories,
      keywords,
      metas,
    });
  } catch (err) {
    errorLogController.handle(req, res, err, 'PAGE');
  }
});

/**
 * Privacy Policy
 */
app.get(`${i18n.__('footer.privacyLink')}/`, async (req, res) => {
  try {
    const metas = await Seo.getMetas(
      `${CONFIG.WEB_TITLE} | ${i18n.__('footer.privacyText')}`,
      `${CONFIG.WEB_TITLE} | ${i18n.__('footer.privacyText')}`,
      CONFIG.OG_ARTICLE,
      true,
      req,
    );

    res.render('privacy', { metas });
  } catch (err) {
    errorLogController.handle(req, res, err, 'PAGE');
  }
});

/**
 * Disclaimer
 */
app.get(`${i18n.__('footer.legalLink')}/`, async (req, res) => {
  try {
    const metas = await Seo.getMetas(
      `${CONFIG.WEB_TITLE} | ${i18n.__('footer.legalText')}`,
      `${CONFIG.WEB_TITLE} | ${i18n.__('footer.legalText')}`,
      CONFIG.OG_ARTICLE,
      true,
      req,
    );

    res.render('disclaimer', { metas });
  } catch (err) {
    errorLogController.handle(req, res, err, 'PAGE');
  }
});

/**
 * Cookies Policy
 */
app.get(`${i18n.__('footer.cookieLink')}/`, async (req, res) => {
  try {
    const metas = await Seo.getMetas(
      `${CONFIG.WEB_TITLE} | ${i18n.__('footer.cookieText')}`,
      `${CONFIG.WEB_TITLE} | ${i18n.__('footer.cookieText')}`,
      CONFIG.OG_ARTICLE,
      true,
      req,
    );

    res.render('cookies', { metas });
  } catch (err) {
    errorLogController.handle(req, res, err, 'PAGE');
  }
});

module.exports = app;
