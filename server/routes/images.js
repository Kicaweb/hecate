/* global CONFIG */

const express = require('express');
const app = express();

const path = require('path');

const Logger = require('../common/Logger');
const Helper = require('../common/Helper');
const Images = require('../controllers/Images');
const Catalog = require('../controllers/Catalog');
const errorLogController = require('../controllers/ErrorLog');

app.get('/image/:slug', async (req, res) => {
  try {
    const slug = req.params.slug.split('.')[0];

    // The old images with this keywords are deprecated
    if (slug.match(/2012|2013|2014|2015|2016|2017/g)) {
      await Helper.prepareError('GONE', 'URL no longer exists', 410);
    }

    const generatedImage = Images.createImageText(slug, false);
    generatedImage.then(
      (result) => {
        res.contentType('image/png');
        res.send(result);
      },
      (err) => {
        Logger.warn('/image/', req, err);
        const noImage = path.resolve(`./${CONFIG.IMG_GEN_IMG_NOT_FOUND}`);
        res.sendFile(noImage);
      },
    );
  } catch (err) {
    errorLogController.handle(req, res, err, 'IMAGE');
  }
});

app.get('/catalog-image/:slug', async (req, res) => {
  try {
    const slug = req.params.slug.split('.')[0];

    // The old images with this keywords are deprecated
    if (slug.match(/2012|2013|2014|2015|2016|2017/g)) {
      await Helper.prepareError('GONE', 'URL no longer exists', 410);
    }

    const keyword = await Catalog.getKeywordBySlug(slug, true);
    if (!keyword) await Catalog.saveKeyword(slug, true);

    let generatedImage;
    if (!keyword) {
      const newKeyword = await Images.createKeywordForImage(slug);
      generatedImage = Images.createImageText(newKeyword, true);
    } else {
      generatedImage = Images.createImageText(keyword, true);
    }

    generatedImage.then(
      (result) => {
        res.contentType('image/png');
        res.send(result);
      },
      (err) => {
        Logger.warn('/catalog-image/', req, err);
        const noImage = path.resolve(`./${CONFIG.IMG_GEN_IMG_NOT_FOUND}`);
        res.sendFile(noImage);
      },
    );
  } catch (err) {
    errorLogController.handle(req, res, err, 'IMAGE');
  }
});

app.get('/image-text/:text', async (req, res) => {
  try {
    const keyword = {};
    keyword.text = req.params.text.split('.')[0];
    keyword._id = 1;
    const generatedImage = Images.createImageText(keyword, true);
    generatedImage.then(
      (result) => {
        res.contentType('image/png');
        res.send(result);
      },
      (err) => {
        Logger.warn('/image-text/', req, err);
        const noImage = path.resolve(`./${CONFIG.IMG_GEN_IMG_NOT_FOUND}`);
        res.sendFile(noImage);
      },
    );
  } catch (err) {
    errorLogController.handle(req, res, err, 'IMAGE');
  }
});

module.exports = app;
