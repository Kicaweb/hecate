const express = require('express');
const app = express();

const Logger = require('../common/Logger');

app.use(require('./redirects'));
app.use(require('./sitemap'));
app.use(require('./rss'));
app.use(require('./content'));
app.use(require('./category'));
app.use(require('./images'));
app.use(require('./search'));
app.use(require('./catalog'));
app.use(require('./scholarship'));
app.use(require('./newsletter'));

/**
 * 404 Error Page
 */
app.use((req, res) => {
  if (req.accepts('html') && res.status(404)) {
    Logger.exception('404', req, '');
    res.render('404');
  }
});

module.exports = app;
