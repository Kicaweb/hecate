/* global CONFIG */

const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
const axios = require('axios');
const errorLogController = require('../controllers/ErrorLog');

app.post('/insertUser', async (req, res) => {
  const userData = req.body;
  userData.webCode = CONFIG.WEBCODE;
  userData.IP = req.headers['x-forwarded-for'];
  try {
    const token = jwt.sign(userData, CONFIG.SECRET_KW);

    const { data } = await axios({
      method: 'post',
      url: `${CONFIG.SERVER_KARDIA}/newsletter/insertUser?token=${CONFIG.AUTH_TOKEN}`,
      headers: { Authorization: token },
      data: userData,
    });
    res.json(data);
  } catch (err) {
    errorLogController.handle(req, res, err, 'NEWSLETTER');
  }
});

module.exports = app;
