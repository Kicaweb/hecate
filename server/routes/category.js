/* global CONFIG */

const express = require('express');
const app = express();

const paginate = require('express-paginate');
app.use(paginate.middleware(CONFIG.PAGINATION_LIMIT, CONFIG.PAGINATION_LIMIT));

const i18n = require('i18n');

const errorLogController = require('../controllers/ErrorLog');
const Helper = require('../common/Helper');
const Category = require('../controllers/Category');
const Catalog = require('../controllers/Catalog');
const Scholarship = require('../controllers/Scholarship');
const Seo = require('../controllers/Seo');

const categoryBase = CONFIG.CATEGORY_BASE;
const urlRegex = new RegExp(`/${categoryBase}/([^/]+)/?(.*)?`);

app.get(urlRegex, async (req, res) => {
  const slug = req.params[0];
  const extraParams = req.params[1];
  try {
    // Redirigimos a la base de categoría cualquier parámetro extra (no parametros GET)
    if ((extraParams && extraParams !== 'amp/') || !req.originalUrl.endsWith('/')) {
      await Helper.prepareError('REDIRECT', `Redirecting: ${req.originalUrl}`, 301, {
        redirect: `/${categoryBase}/${slug}/`,
      });
    }
    const [category, keywords] = await Promise.all([Category.getBySlug(slug), Catalog.getSidebarKeywords()]);
    const [scholarships, scholarshipCount] = await Promise.all([
      Scholarship.getAllByCategorySlugWithLimit(category, req.query.skip, req.query.limit),
      Scholarship.countScholarshipsByCategory(category),
    ]);
    const [pageCount, pages] = await Helper.preparePagination(scholarshipCount, paginate, req);

    let isAmp = false;
    let urlToRender = 'category';

    if (extraParams === 'amp/') {
      urlToRender = 'amp/category';
      isAmp = true;
    }

    // * TEMPORAL HASTA QUE TODAS LAS CATEGORÍAS TENGAN SEO
    // * Generate seo title and metatitle if the category doesn't have

    category.seo.metaTitle = category.seo.metaTitle ? category.seo.metaTitle : category.name;
    category.seo.metaDescription = category.seo.metaDescription
      ? category.seo.metaDescription
      : i18n.__('category.metaDescription').replace('[CATEGORY]', category.name);

    const metas = await Seo.getMetas(
      category.seo.metaTitle,
      category.seo.metaDescription,
      CONFIG.OG_WEBSITE,
      category.seo.noIndex,
      req,
      isAmp,
      'category',
    );

    res.render(urlToRender, {
      metas,
      keywords,
      category,
      scholarships,
      pageCount,
      pages,
    });
  } catch (err) {
    errorLogController.handle(req, res, err, 'CATEGORY');
  }
});

module.exports = app;
