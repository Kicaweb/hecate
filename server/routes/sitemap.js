/* global CONFIG */

const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');
const replace = require('replace-in-file');
const sm = require('sitemap');
const i18n = require('i18n');

const Scholarship = require('../controllers/Scholarship');
const Category = require('../controllers/Category');
const Helper = require('../common/Helper');
const errorLogController = require('../controllers/ErrorLog');

app.get('/create-sitemap', async (req, res) => {
  try {
    const [scholarshipsList, [parentCategoryList, childrenCategoryList]] = await Promise.all([
      Scholarship.getSlugsFromAll(),
      Category.getSlugsForSitemap(),
    ]);

    const urls = await Helper.createUrlsSitemap(scholarshipsList, parentCategoryList, childrenCategoryList);

    const sitemap = sm.createSitemap({
      hostname: CONFIG.URL,
      cacheTime: 600000, // 600 sec - cache purge period
      urls: Array.from(urls),
    });
    sitemap.toXML((err, xml) => {
      if (err) {
        return res.status(500).end();
      }
      fs.writeFile(`${__dirname}/../../assets/sitemap.xml`, xml, (error) => {
        if (error) {
          errorLogController.handle(req, res, error, 'SITEMAP');
        }
      });
      return res.json({ result: 'OK' });
    });
  } catch (err) {
    errorLogController.handle(req, res, err, 'SITEMAP');
  }
});

app.get('/sitemap.xml', async (req, res) => {
  try {
    const pathSitemap = path.resolve(__dirname, '../../assets/sitemap.xml');
    const sitemap = fs.readFileSync(pathSitemap);
    res.contentType('text/plain');
    res.send(sitemap);
  } catch (err) {
    errorLogController.handle(req, res, err, 'SITEMAP');
  }
});

app.get('/robots.txt', async (req, res) => {
  try {
    const pathRobots = path.resolve(__dirname, '../../assets/robots.txt');
    const options = {
      files: pathRobots,
      from: ['[LEGAL]', '[COOKIES]', '[PRIVACIDAD]'],
      to: [i18n.__('footer.legalLink'), i18n.__('footer.cookieLink'), i18n.__('footer.privacyLink')],
    };
    await replace(options);
    const robots = fs.readFileSync(pathRobots);
    res.contentType('text/plain');

    res.send(robots);
  } catch (err) {
    errorLogController.handle(req, res, err, 'ROBOTS');
  }
});

app.get('/ads.txt', (req, res) => {
  res.render('ads');
});

module.exports = app;
