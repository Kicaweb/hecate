/* global CONFIG */

const express = require('express');
const app = express();

const session = require('express-session');
app.use(
  session({
    secret: 'search terms',
    resave: false,
    saveUninitialized: false,
  }),
);

const slugify = require('slugify');

const i18n = require('i18n');

const Scholarship = require('../controllers/Scholarship');
const Catalog = require('../controllers/Catalog');
const Seo = require('../controllers/Seo');
const errorLogController = require('../controllers/ErrorLog');

const searchActionName = i18n.__('searchEngine.actionName');

app.post(`/${searchActionName}/`, (req, res) => {
  const searchTerm = req.body.srch_term;
  req.session.string_search = searchTerm;
  res.redirect(`/${searchActionName}/${slugify(searchTerm, { remove: /[$*+,~()'"!:@]/g, lower: true })}`);
});

app.get(`/${searchActionName}/:slug`, async (req, res) => {
  try {
    let searchTerm = req.session.string_search;
    if (searchTerm === undefined) {
      const repSlug = new RegExp('-', 'g');
      searchTerm = req.params.slug.replace(repSlug, ' ');
    }

    const [keywords, scholarships, metas] = await Promise.all([
      Catalog.getSidebarKeywords(),
      Scholarship.searchByTerm(searchTerm),
      Seo.getMetas(
        `Your ${searchActionName}: ${searchTerm}`,
        `Your ${searchActionName}: ${searchTerm}`,
        CONFIG.OG_WEBSITE,
        true,
        req,
        false,
      ),
    ]);

    res.render('search', {
      paginate: false,
      scholarships,
      cadena: searchTerm,
      metas,
      keywords,
    });
  } catch (err) {
    errorLogController.handle(req, res, err, 'SEARCH');
  }
});

module.exports = app;
