const express = require('express');
const app = express();

const Rss = require('../controllers/Rss');
const errorLogController = require('../controllers/ErrorLog');

app.get('/rss/', async (req, res) => {
  try {
    const feed = await Rss.prepareRss();

    res.set('Content-Type', 'text/xml');
    res.send(feed.rss2());
  } catch (err) {
    errorLogController.handle(req, res, err, 'RSS');
  }
});
app.get('/atom/', async (req, res) => {
  try {
    const feed = await Rss.prepareRss();
    res.set('Content-Type', 'text/plain');
    res.send(feed.atom1());
  } catch (err) {
    errorLogController.handle(req, res, err, 'RSS');
  }
});
app.get('/json/', async (req, res) => {
  try {
    const feed = await Rss.prepareRss();
    res.json(feed.json1());
  } catch (err) {
    errorLogController.handle(req, res, err, 'RSS');
  }
});

module.exports = app;
