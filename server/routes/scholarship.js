/* global CONFIG */

const express = require('express');
const app = express();

const Scholarship = require('../controllers/Scholarship');
const Catalog = require('../controllers/Catalog');
const errorLogController = require('../controllers/ErrorLog');
const Seo = require('../controllers/Seo');
// const Helper = require('../common/Helper');

app.get(/^\/(.*?)\/(.*)\/?$/, async (req, res) => {
  const slug = req.params[0];
  const extraParams = req.params[1];
  try {
    // This masks potential errors (It is redirecting /wp-content/themes/ to /wp-content/)
    // if (extraParams && extraParams !== 'amp/') {
    //   await Helper.prepareError('REDIRECT', `Redirecting: ${slug}/${req.params[1]}`, 301, { redirect: `/${slug}/` });
    // }

    const scholarship = await Scholarship.getBySlug(slug);

    let isAmp = false;
    let urlToRender = 'scholarship';

    if (extraParams === 'amp/') {
      urlToRender = 'amp/scholarship';
      isAmp = true;
    }

    const [keywords, metas] = await Promise.all([
      Catalog.getSidebarKeywords(),
      Seo.getMetas(
        scholarship.title,
        `${scholarship.description.substring(0, 130)}...`,
        CONFIG.OG_ARTICLE,
        scholarship.seo.noIndex,
        req,
        isAmp,
        'scholarship',
      ),
    ]);

    const category = scholarship.categories[0];

    res.render(urlToRender, {
      metas,
      scholarship,
      keywords,
      category,
    });
  } catch (err) {
    errorLogController.handle(req, res, err, 'SCHOLARSHIP');
  }
});

module.exports = app;
