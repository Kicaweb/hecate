const express = require('express');
const app = express();

app.get('/sp-push-manifest.json', (req, res) => {
  res.header('Content-Type', 'application/json');
  res.render('sendpulse/sp-push-manifest');
});
app.get('/sp-push-worker-fb.js', (req, res) => {
  res.header('Content-Type', 'application/javascript');
  res.render('sendpulse/sp-push-worker-fb');
});

module.exports = app;
