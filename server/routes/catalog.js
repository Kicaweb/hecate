/* global CONFIG */

const express = require('express');
const app = express();
const useragent = require('express-useragent');

const Helper = require('../common/Helper');
const Scholarship = require('../controllers/Scholarship');
const Catalog = require('../controllers/Catalog');

const errorLogController = require('../controllers/ErrorLog');
const Seo = require('../controllers/Seo');

const catalogBase = CONFIG.CATALOG_BASE;
const urlRegex = new RegExp(`/${catalogBase}/(.*?)/?(.*)?`);

app.get(urlRegex, async (req, res) => {
  const extraParams = req.params[1];
  const source = req.headers['user-agent'];
  const ua = useragent.parse(source);
  try {
    // The catalog pages with this keywords are deprecated
    // if (extraParams.match(/2012|2013|2014|2015|2016|2017|more/g)) {
    //   await Helper.prepareError('GONE', 'URL no longer exists', 410);
    // }
    const slug = extraParams.split('/')[0];
    // Redirigimos a la base del catalogo cualquier parámetro extra (no parametros GET)
    if (extraParams.split('/')[1]) {
      await Helper.prepareError('REDIRECT', `Redirecting: ${req.originalUrl}`, 301, {
        redirect: `/${catalogBase}/${slug}/`,
      });
    }
    let keyword = await Catalog.getKeywordBySlug(slug, false);

    if (!keyword && ua.isBot === false) {
      keyword = await Catalog.saveKeyword(slug, false);
    } else if (!keyword && ua.isBot !== false) {
      keyword = await Catalog.prepareKeywordBot(slug);
    }

    const code = await Catalog.getUniqueCode(slug);

    const [keywords, [texts, subtitle, description], video, scholarships] = await Promise.all([
      Catalog.getSidebarKeywords(),
      Catalog.getTexts(keyword, code),
      Catalog.getVideo(code),
      Scholarship.getForCatalog(slug),
    ]);
    const metas = await Seo.getMetas(
      keyword.text,
      `${subtitle}. ${description}`,
      CONFIG.OG_WEBSITE,
      false,
      req,
      false,
      'catalog',
    );

    res.render('catalog', {
      paginate: false,
      scholarships,
      keyword,
      keywords,
      metas,
      subtitle,
      video,
      texts,
      isCatalog: true,
    });
  } catch (err) {
    errorLogController.handle(req, res, err, 'CATALOG');
  }
});

module.exports = app;
