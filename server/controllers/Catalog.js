/* global CONFIG */
const i18n = require('i18n');
const _ = require('lodash');
const slugify = require('slugify');

const KeywordValid = require('../models/keywordValid');
const Helper = require('../common/Helper');
const Logger = require('../common/Logger');

class Catalog {
  /**
   *
   * Generates a random number basen in the keyword
   * @static
   * @param String keyword
   * @returns a Number based in the keyword
   */
  static async hashCode(keyword) {
    let hash = 0;
    let i;
    let chr;
    let len;
    if (keyword.length === 0) return hash;
    for (i = 0, len = keyword.length; i < len; i += 1) {
      chr = keyword.charCodeAt(i);
      hash = (hash << 5) - hash + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

  /**
   * Gets the data from a category given some filter
   *
   * @static
   * @param {*} filter Filter for the query
   * @param {*} options Query options
   * @returns a KeywordValid object
   * @example
   * getKeyword({ filter: { slug: 'my-slug' }, fields: {}})
   */
  static async getKeyword(filter, options) {
    const resultFilter = _.isEmpty(filter) ? {} : filter;
    const { fields } = options;
    const keyword = await KeywordValid.findOne(resultFilter, fields).lean();

    return keyword;
  }

  /**
   * Gets the keywords given some filter and options
   *
   * @param {Array} filter Filter for the query
   * @param {Array} options Query options: fields of the model, limit, sort or skip
   * @returns A Category object array
   * @example
   * getKeywords({filter: { slug: 'my-slug' },
   *                  options: {
   *                          fields: {},
   *                          limit: 10,
   *                          skip: 10,
   *                          sort: { publishDate: 1 }
   *                  }
   *                });
   */
  static async getKeywords(filter, options = {}) {
    const resultFilter = _.isEmpty(filter) ? {} : filter;
    const fields = _.isEmpty(options.fields) ? {} : options.fields;
    const limit = options.limit > 0 ? options.limit : parseInt(CONFIG.PAGINATION_LIMIT, 10);
    const { sort, skip } = options;
    const keywords = await KeywordValid.find(resultFilter, fields)
      .limit(limit)
      .skip(skip)
      .sort(sort)
      .lean();

    return keywords;
  }

  /**
   * Returns the 20 last catalog keywords for the sidebar
   *
   * @returns An array of KeywordValid objects
   */
  static async getSidebarKeywords() {
    const filter = { webCode: CONFIG.WEBCODE };
    const options = {
      fields: {
        slug: 1,
        text: 1,
      },
      limit: 20,
      sort: { lastAccess: -1 },
    };

    const keywords = await this.getKeywords(filter, options);

    if (_.isEmpty(keywords)) {
      Logger.warn('SIDEBAR', { description: 'There is no Sidebar Keywords' });
    }

    return keywords;
  }

  /**
   * Returns a valid keyword which matches the request slug
   *
   * @param {String} slug Http request variable parameter
   * @param {Boolean} isImage A flag to know if this method is called from Image route or not
   * @returns A KeywordValid object
   */
  static async getKeywordBySlug(slug, isImage) {
    let text;
    if (isImage) {
      text = new RegExp(
        slug
          .replace(/-/g, ' ')
          .replace(/\+/g, ' ')
          .replace('.jpg', ''),
        'gi',
      );
    } else {
      text = new RegExp(slug.replace(/-/g, ' ').replace(/\+/g, ' '), 'gi');
    }

    const filter = { $or: [{ slug }, { text }] };
    const options = {
      fields: {
        _id: 1,
        text: 1,
        slug: 1,
      },
    };

    const keyword = await this.getKeyword(filter, options);
    return keyword;
  }

  /**
   * Save a new Keyword in the database
   *
   * @param {String} slug The keyword to save
   * @returns A KeywordValid object
   */
  static async prepareKeywordBot(slug) {
    const keyword = new KeywordValid();
    keyword.text = slug.replace('-', ' ').replace('+', ' ');
    keyword.slug = slug;
    return keyword;
  }

  /**
   * Save a new Keyword in the database
   *
   * @param {String} keyword The keyword to save
   * @param {Boolean} isImage A flag to know if this method is called from Image route or not
   * @returns A KeywordValid object
   */
  static async saveKeyword(keyword, isImage) {
    let text;
    if (isImage) {
      text = keyword
        .replace(/-/g, ' ')
        .replace(/\+/g, ' ')
        .replace('.jpg', '');
    } else {
      text = keyword.replace(/-/g, ' ').replace(/\+/g, ' ');
    }
    const now = new Date();
    const keywordValid = new KeywordValid({
      text,
      slug: slugify(text, {
        replacement: '-',
        remove: /[+.()'"!:@]/g,
        lower: true,
      }),
      lastAccess: now.setMonth(now.getMonth() - 1),
      webCode: CONFIG.WEBCODE,
    });

    const keywordSaved = await keywordValid.save();

    if (!keywordSaved) {
      await Helper.prepareError('CATALOG', `Error saving the '${keyword}' keyword`, 404);
    }
    return keywordSaved;
  }

  /**
   * Returns an unique code using the slug parameter
   * to ensure that the texts and videos will be always the same for
   * the same keyword
   *
   * @static
   * @param {String} slug Http request variable parameter
   * @returns An unique number
   */
  static async getUniqueCode(slug) {
    const codeSlug = await this.hashCode(slug);
    const code = Number(`0.${Math.abs(codeSlug)}`);
    return code;
  }

  /**
   * Returns a description, a subtitle and 7 spinned texts to render in the page.
   * Always returns the same array of texts given the same keyword.
   * These spyntax can be found in .env file
   *
   * @param {*} keyword The keyword used to render the page
   * @param {*} code An unique number generated for that keyword
   * @see Catalog#getUniqueCode
   * @returns An array of Strings containing spinned texts
   */
  static async getTexts(keyword, code) {
    const result = [];
    const texts = [];
    const regex = new RegExp(CONFIG.SPECIAL_WORD, 'g');

    const spins = i18n.__('spins');
    const [text1, text2, text3, text4, text5, text6, text7, subtitle, description] = await Promise.all([
      Helper.spin(spins.spin01.replace(regex, keyword.text), code),
      Helper.spin(spins.spin02.replace(regex, keyword.text), code),
      Helper.spin(spins.spin03.replace(regex, keyword.text), code),
      Helper.spin(spins.spin04.replace(regex, keyword.text), code),
      Helper.spin(spins.spin05.replace(regex, keyword.text), code),
      Helper.spin(spins.spin06.replace(regex, keyword.text), code),
      Helper.spin(spins.spin07.replace(regex, keyword.text), code),
      Helper.spin(spins.spinSubtitle.replace(regex, keyword.text), code),
      Helper.spin(spins.spinDescription.replace(regex, keyword.text), code),
    ]);
    texts.push(text1, text2, text3, text4, text5, text6, text7);
    result.push(texts, subtitle, description);
    return result;
  }

  /**
   * Returns a Youtube video ID to render in the page.
   * Always returns the same ID given the same keyword.
   * The video ID source can be found in .env file
   *
   * @param {*} code An unique number generated for that keyword
   * @see Catalog#getUniqueCode
   * @returns A Youtube video ID
   */
  static async getVideo(code) {
    const spins = i18n.__('spins.videos');
    const videos = spins.split(',');
    return videos[Math.floor(code * videos.length)];
  }
}

module.exports = Catalog;
