/* global CONFIG */
const _ = require('lodash');
const ScholarshipDb = require('../models/scholarship');
const ScholarshipWeb = require('../models/scholarshipWeb');
const Helper = require('../common/Helper');
const Logger = require('../common/Logger');

class Scholarship {
  /**
   * Gets the data from a scholarship given some filter
   *
   * @param {*} filter Filter for the query
   * @param {*} options Query options
   * @returns A Scholarship Web object
   * @example
   * getScholarship({ filter: { slug: 'my-slug' }, fields: {}})
   */
  static async getScholarship(filter, options = {}) {
    const resultFilter = _.isEmpty(filter) ? {} : filter;
    const fields = _.isEmpty(options.fields)
      ? { 'webData.$': 1, source: 1, sponsor: 1, amount: 1, contact: 1 }
      : options.fields;
    const scholarship = await ScholarshipDb.findOne(resultFilter, fields).lean();

    if (scholarship) {
      const scholarshipWeb = ScholarshipWeb.prepare(scholarship);
      return scholarshipWeb;
    }
    return null;
  }

  /**
   * Gets the scholarships given some filter and options
   *
   * @param {Array} filter Filter for the query
   * @param {Array} options Query options: fields of the model, limit, sort or skip
   * @returns A Scholarship Web object array
   * @example
   * getScholarships({filter: { slug: 'my-slug' },
   *                  options: {
   *                          fields: {},
   *                          limit: 10,
   *                          skip: 10,
   *                          sort: { publishDate: 1 }
   *                  }
   *                });
   */
  static async getScholarships(filter, options = {}) {
    const resultFilter = _.isEmpty(filter) ? {} : filter;
    const fields = _.isEmpty(options.fields) ? { 'webData.$': 1 } : options.fields;

    const limit = options.limit > 0 ? options.limit : parseInt(CONFIG.PAGINATION_LIMIT, 10);
    const { sort, skip } = options;
    const scholarships = await ScholarshipDb.find(resultFilter, fields)
      .limit(limit)
      .skip(skip)
      .sort(sort)
      .lean();
    const scholarshipsWeb = [];
    await scholarships.forEach((scholarship) => {
      scholarshipsWeb.push(ScholarshipWeb.prepare(scholarship));
    });

    return scholarshipsWeb;
  }

  /**
   *
   * Count the scholarships in the database of a query
   *
   * @param {Object} filter[] - Filter for the query
   * @returns Number of scholarships
   */
  static async countScholarships(filter) {
    const resultFilter = _.isEmpty(filter) ? {} : filter;
    const scholarships = await ScholarshipDb.countDocuments(resultFilter);
    return scholarships;
  }

  /**
   * Gets the data from a scholarship given its slug
   *
   * @param {String} slug The Scholarship slug
   * @returns A ScholarshipWeb object
   */
  static async getBySlug(slug) {
    const filter = {
      webData: {
        $elemMatch: {
          webCode: CONFIG.WEBCODE,
          slug,
          publishDate: {
            $lte: new Date(),
          },
        },
      },
    };
    const options = {
      filter,
      fields: {
        'webData.$': 1,
        source: 1,
        sponsor: 1,
        amount: 1,
        duration: 1,
        contact: 1,
      },
    };

    const scholarship = await this.getScholarship(filter, options);
    if (!scholarship) {
      await Helper.prepareError('NOT FOUND', 'Scholarship URL not found', 404);
    }

    return scholarship;
  }

  /**
   * Gets the number of scholarships of a Category
   *
   * @param {Object} category The Category Object
   * @returns A number
   */
  static async countScholarshipsByCategory(category) {
    const filter = {
      webData: {
        $elemMatch: {
          'categories.slug': category.slug,
          webCode: CONFIG.WEBCODE,
          publishDate: {
            $lte: new Date(),
          },
        },
      },
    };

    const scholarshipsNumber = await this.countScholarships(filter);

    if (scholarshipsNumber < 1 && !this.isCategoryParent(category)) {
      await Helper.prepareError('ERROR CONTENT', `This Category: '${category.slug}', has no scholarships`, 404);
    }
    return scholarshipsNumber;
  }

  /**
   * Gets all the scholarships for a category. The limit is used for pagination purposes.
   * Only returns the webData of the webCode provided
   *
   * @param {Object} slug The Category Object
   * @param {Number} skip Pagination param given in the HTTP request
   * @param {Number} [limit=CONFIG.PAGINATION_LIMIT] Pagination param given in the HTTP request
   * @returns An array of Scholarship Objects
   */
  static async getAllByCategorySlugWithLimit(category, skip, limit = CONFIG.PAGINATION_LIMIT) {
    const filter = {
      webData: {
        $elemMatch: {
          'categories.slug': category.slug,
          webCode: CONFIG.WEBCODE,
          publishDate: {
            $lte: new Date(),
          },
        },
      },
    };
    const options = {
      fields: { 'webData.$': 1, duration: 1 },
      limit,
      skip,
      sort: { insertDate: 'descending' },
    };

    const scholarships = await this.getScholarships(filter, options);
    scholarships.sort((a, b) => {
      return new Date(b.publishDate) - new Date(a.publishDate);
    });
    if (_.isEmpty(scholarships) && !this.isCategoryParent(category)) {
      await Helper.prepareError(
        'ERROR CONTENT',
        `There was an error retrieving scholarships for the '${category.slug}' Category`,
        404,
      );
    }
    return scholarships;
  }

  /**
   * This method will return the top 20 scholarships which better match with the query of the user
   *
   * @param {String} term User provided query
   * @returns An array of Scholarship Objects
   */
  static async searchByTerm(term) {
    const regExpStringSearch = new RegExp(term, 'ig');

    const filter = {
      $text: {
        $search: regExpStringSearch,
      },
      webData: {
        $elemMatch: {
          webCode: CONFIG.WEBCODE,
          publishDate: {
            $lte: new Date(),
          },
        },
      },
    };
    const options = {
      fields: {
        'webData.$': 1,
        score: {
          $meta: 'textScore',
        },
      },
      limit: 20,
      sort: {
        score: {
          $meta: 'textScore',
        },
      },
    };
    const scholarships = await this.getScholarships(filter, options);
    if (_.isEmpty(scholarships)) {
      Logger.warn('SEARCH', { description: `The search text '${term}' has not scholarships results` });
    }
    return scholarships;
  }

  /**
   * This method will return the top 8 scholarships given a slug
   *
   * @param {*} slug Slug from Catalog section
   * @returns An array of Scholarship Objects
   */
  static async getForCatalog(slug) {
    let regex = new RegExp('(more|2018|2019|2020)', 'ig');
    let stringNoYears = slug.replace(regex, '');
    regex = new RegExp('(\\++)', 'ig');
    stringNoYears = stringNoYears.replace(regex, ' ').trim();
    const filter = {
      $text: {
        $search: stringNoYears,
      },
      webData: {
        $elemMatch: {
          webCode: CONFIG.WEBCODE,
          publishDate: {
            $lte: new Date(),
          },
        },
      },
    };
    const options = {
      fields: {
        'webData.$': 1,
        score: {
          $meta: 'textScore',
        },
      },
      limit: 8,
      sort: {
        score: {
          $meta: 'textScore',
        },
      },
    };
    const scholarships = await this.getScholarships(filter, options);
    if (_.isEmpty(scholarships)) {
      Logger.warn('CATALOG', { description: `The term '${slug}' has not scholarships results` });
    }
    return scholarships;
  }

  /**
   * Returns the slugs of the scholarships to be inserted in the sitemap.xml
   *
   * @returns An array with all the scholarships slugs
   */
  static async getSlugsFromAll() {
    const params = {
      webData: {
        $elemMatch: {
          webCode: CONFIG.WEBCODE,
          publishDate: { $lte: new Date() },
        },
      },
    };

    const scholarships = await ScholarshipDb.find(params)
      .limit(0)
      .lean();
    const scholarshipsWeb = [];
    await scholarships.forEach((scholarship) => {
      scholarshipsWeb.push(ScholarshipWeb.prepare(scholarship));
    });

    if (_.isEmpty(scholarshipsWeb)) {
      await Helper.prepareError('ERROR CONTENT', 'There was an error retrieving scholarships for the sitemap', 404);
    }
    return scholarshipsWeb;
  }

  /**
   * Check if this category is parent
   *
   * @param {Object} category The Category Object
   * @returns boolean
   */
  static isCategoryParent(category) {
    return category.subCategories.length > 0;
  }
}
module.exports = Scholarship;
