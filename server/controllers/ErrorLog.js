/* global CONFIG */
const useragent = require('express-useragent');

const ErrorLogModel = require('../models/errorLog');
const Logger = require('../common/Logger');

class ErrorLog {
  /**
   *
   *
   * @static
   * @param Object req
   * @param Object res
   * @param Object err
   * @param String section
   * @param Array [params={}]
   * @memberof ErrorLog
   */
  static async handle(req, res, err, section) {
    switch (err.name) {
      case 'REDIRECT':
        // if is a redirect don't save
        // await this.save(req, err, section);
        Logger.log(section, req, err);
        res.redirect(301, err.redirect);
        break;
      case 'GONE':
        // if is a redirect don't save
        // await this.save(req, err, section);
        Logger.log(section, req, err);
        res.status(410).render('404');
        break;
      default:
        // Removed functionality: Write in DB the result of the log
        // await this.save(req, err, section);
        Logger.exception(section, req, err);
        res.status(404).render('404');
        break;
    }
  }

  static async save(req, err, section) {
    const source = req.headers['user-agent'];
    const ua = useragent.parse(source);
    const error = new ErrorLogModel();
    error.webCode = CONFIG.WEBCODE;
    error.section = section;
    error.action = err.name;
    error.description = err.description;
    error.ip = req.headers['x-forwarded-for'];
    error.url = req.originalUrl;
    error.userAgent = JSON.stringify(ua);
    error.bot = String(ua.isBot);
    if (err) if (err.stack) error.stack = err.stack;
    error.responseCode = err.number;
    error.save();
  }
}
module.exports = ErrorLog;
