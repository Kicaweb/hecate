/* global CONFIG */

const fs = require('fs');

const uniqid = require('uniqid');
const text2png = require('text2png');
const Jimp = require('jimp');

const Scholarship = require('./Scholarship');

class Images {
  /**
   * Creates a keyword object required in the createImageText method
   * from a slug provided in the URL
   *
   * @static
   * @param {String} slug The text that will appear on the image
   * @returns A KeywordValid object
   */
  static async createKeywordForImage(slug) {
    const keyword = {};
    keyword.text = slug.replace(/-/g, ' ');
    keyword._id = uniqid();
    return keyword;
  }

  /**
   * Creates an unique image based on the text of a keyword.
   * This method selects one image of the assets and puts the text inside the image
   *
   * @static
   * @param {*} keyword The text that will appear on the image
   * @param {Boolean} isImage Boolean flag to know if this method is called from Catalog route or not
   * @returns A buffer image
   */
  static async createImageText(keyword, isCatalog) {
    let sentence;
    let id;
    if (isCatalog) {
      sentence = keyword.text;
      id = keyword._id;
    } else {
      const scholarship = await Scholarship.getBySlug(keyword);
      if (scholarship) {
        sentence = scholarship.title;
        id = scholarship._id;
      } else {
        sentence = keyword.replace(/-/g, ' ');
        id = uniqid();
      }
    }
    const words = sentence.split(' ');

    const lines = [];
    let finalSentence = '';
    let counter = 0;

    words.forEach((word) => {
      counter += 1;
      if (finalSentence.length + word.length < 22) {
        finalSentence += `${word} `;
      } else if (counter < words.length) {
        lines.push(finalSentence);
        finalSentence = '';
        finalSentence += `${word} `;
      } else if (finalSentence.length + word.length < 22) {
        finalSentence += word;
      } else {
        lines.push(finalSentence);
        finalSentence = '';
        finalSentence += `${word} `;
      }
    });

    lines.push(finalSentence);
    const text = lines.join('\n');

    const pathFreeImages = `./${CONFIG.IMG_GEN_PATH}`;
    const randomNumber = String(Number(String(id).replace(/\D/g, '') * sentence.length)).substr(0, 4);
    const items = fs.readdirSync(pathFreeImages);
    let item = pathFreeImages + items[Math.round((randomNumber.substr(-2) / 100) * items.length - 1)];
    if (!item.includes('jpg')) {
      item = pathFreeImages + items[Math.round((randomNumber.substr(-2) / 100) * items.length - 1) - 5];
    }
    const bufferT = text2png(text, {
      font: CONFIG.IMG_GEN_FONT,
      localFontPath: `./${CONFIG.IMG_GEN_FONT_PATH}`,
      localFontName: CONFIG.IMG_GEN_FONT_NAME,
      color: CONFIG.IMG_GEN_COLOR,
      backgroundColor: CONFIG.IMG_GEN_BACKGROUND_COLOR,
      lineSpacing: 10,
      padding: 30,
      paddingBottom: 40,
      output: 'buffer',
    });

    let offset = text.length;
    if (text.length > 99) {
      offset -= 99;
    }

    if (item) {
      return new Promise(async (resolve, reject) => {
        const textForImage = await Jimp.read(bufferT);
        const finalImage = await Jimp.read(item);
        finalImage
          .resize(1200, Jimp.AUTO)
          .crop(0, 100 - offset, 1200, 500)
          .composite(textForImage, 440, 0)
          .quality(90)
          .getBuffer(Jimp.MIME_JPEG, (error, buffer) => {
            if (error) {
              reject(error);
            } else {
              resolve(buffer);
            }
          });
      });
    }
    return Promise.reject(new Error('item is undefined'));
  }
}

module.exports = Images;
