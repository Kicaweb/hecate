/* global CONFIG */

const Helper = require('../common/Helper');

class Seo {
  /**
   * This method will compose an object with valuable SEO data to be rendered
   *
   * @param {String} title Metatitle of the page
   * @param {String} description MetaDescription of the page
   * @param {String} type A string containing one valid value from .env file
   * @param {Boolean} noIndex A flag indicating the noindex parameter for this page
   * @param {String} req The HTTP request
   * @param {Boolean} amp A flag to know if this is an AMP page
   * @param {String} pageType A flag to know what type of page is
   * @returns An object which includes all the SEO data
   */
  static async getMetas(title, description, type, noIndex, req, amp, pageType = '') {
    let fullUrl;
    let urlWithoutParams;
    switch (pageType) {
      case 'category':
        fullUrl = `${CONFIG.URL}${CONFIG.CATEGORY_BASE}/${req.params[0]}/`;
        break;
      case 'catalog':
        fullUrl = `${CONFIG.URL}${CONFIG.CATALOG_BASE}/${req.params[0]}/`;
        break;
      case 'scholarship':
        fullUrl = `${CONFIG.URL}${req.params[0]}/`;
        break;
      default:
        urlWithoutParams = await Helper.removeGetParamsFromUrl(req.originalUrl);
        fullUrl = CONFIG.URL + urlWithoutParams;
        break;
    }

    const metas = {
      title,
      description,
      type,
      noIndex,
      url: fullUrl,
      canonical: fullUrl,
      amp,
    };
    return metas;
  }
}

module.exports = Seo;
