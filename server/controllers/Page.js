/* global CONFIG */

const PageDb = require('../models/page');
const Helper = require('../common/Helper');

class Page {
  /**
   * Gets the data for the homepage
   *
   * @returns a Page object
   */
  static async getHomeContent() {
    const page = await PageDb.findOne(
      { slug: CONFIG.HOME_SPECIAL_SLUG, webCode: CONFIG.WEBCODE },
      {
        seo: 1,
        title: 1,
        content: 1,
        sections: 1,
        descriptions: 1,
      },
    ).lean();
    if (!page) {
      await Helper.prepareError('NOT FOUND', 'Home content not found', 404);
    }
    return page;
  }

  /**
   * Gets the data for a custom page given a slug
   *
   * @param {String} slug Http request variable parameter
   * @returns a Page object
   */
  static async getContent(slug) {
    const page = await PageDb.findOne(
      { slug, webCode: CONFIG.WEBCODE },
      {
        seo: 1,
        title: 1,
        content: 1,
      },
    ).lean();
    if (!page) {
      await Helper.prepareError('NOT FOUND', `Page ${slug} not found`, 404);
    }
    return page;
  }
}

module.exports = Page;
