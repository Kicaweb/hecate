/* global CONFIG */
const _ = require('lodash');
const CategoryDb = require('../models/category');
const Helper = require('../common/Helper');

class Category {
  /**
   * Gets the data from a category given some filter
   *
   * @static
   * @param {*} filter Filter for the query
   * @param {*} options Query options
   * @returns a category object
   * @example
   * getCategory({ filter: { slug: 'my-slug' }, fields: {}})
   */
  static async getCategory(filter, options) {
    const resultFilter = _.isEmpty(filter) ? {} : filter;
    const { fields } = options;
    const scholarship = await CategoryDb.findOne(resultFilter, fields).lean();

    return scholarship;
  }

  /**
   * Gets the categories given some filter and options
   *
   * @param {Array} filter Filter for the query
   * @param {Array} options Query options: fields of the model, limit, sort or skip
   * @returns A Category object array
   * @example
   * getCategories({filter: { slug: 'my-slug' },
   *                  options: {
   *                          fields: {},
   *                          limit: 10,
   *                          skip: 10,
   *                          sort: { publishDate: 1 }
   *                  }
   *                });
   */
  static async getCategories(filter, options = {}) {
    const resultFilter = _.isEmpty(filter) ? {} : filter;
    const fields = _.isEmpty(options.fields) ? {} : options.fields;
    const limit = options.limit > 0 ? options.limit : parseInt(CONFIG.PAGINATION_LIMIT, 10);
    const { sort, skip } = options;
    const categories = await CategoryDb.find(resultFilter, fields)
      .limit(limit)
      .skip(skip)
      .sort(sort)
      .lean();

    return categories;
  }

  /**
   * Returns the slugs of the parents and children categories to be inserted
   * in the sitemap.xml
   *
   * @returns An array with the parent category slugs and the children category slugs
   */
  static async getSlugsForSitemap() {
    const children = [];
    const parents = [];
    const filter = { webCode: CONFIG.WEBCODE, active: true };
    const options = {
      fields: {
        slug: 1,
        parent: 1,
      },
    };
    const categories = await this.getCategories(filter, options);
    categories.forEach((category) => {
      if (category.parent === undefined) {
        parents.push(category);
      } else {
        children.push(category);
      }
    });
    if (_.isEmpty(parents) && _.isEmpty(children)) {
      await Helper.prepareError('ERROR CONTENT', 'There is not categories for the sitemap', 404);
    }
    return [parents, children];
  }

  /**
   *  Gets all the categories without parents from the database and contains featured children
   *
   * @returns An array of Category objects
   */
  static async getHighestParentCategories() {
    const filter = {
      webCode: CONFIG.WEBCODE,
      subCategories: { $exists: true, $elemMatch: { featured: true } },
      parent: { $exists: false },
    };
    const options = {
      fields: {
        subCategories: 1,
        slug: 1,
        name: 1,
      },
    };

    const categories = await this.getCategories(filter, options);
    if (_.isEmpty(categories)) {
      await Helper.prepareError('ERROR CONTENT', 'There is not categories for the Home page', 404);
    }
    return categories;
  }

  /**
   *  Returns a category given a slug
   *
   * @param {String} slug Http request variable parameter
   * @returns A Category object
   */
  static async getBySlug(slug) {
    const filter = { slug, webCode: CONFIG.WEBCODE };
    const options = {
      fields: {
        seo: 1,
        slug: 1,
        name: 1,
        descriptions: 1,
        subCategories: 1,
        scholarshipCount: 1,
      },
    };

    const category = await this.getCategory(filter, options);
    if (!category) {
      await Helper.prepareError('NOT FOUND', `Category '${slug}' not found`, 404);
    }
    return category;
  }
}

module.exports = Category;
