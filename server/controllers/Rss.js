/* global CONFIG */
var mongoose = require('mongoose');
const { Feed } = require('feed');
const Scholarship = require('../controllers/Scholarship');
class Rss {
  static async prepareRss() {
    const feed = new Feed({
      title: `Feed ${CONFIG.HD_IMG_NAME}`,
      description: `Feed for the ${CONFIG.HD_IMG_NAME} website.`,
      id: CONFIG.URL,
      link: CONFIG.URL,
      language: CONFIG.LANGUAGE,
      image: `${CONFIG.URL}img/${CONFIG.HD_IMG_NAME}`,
      favicon: `${CONFIG.URL}${CONFIG.HD_IMG_NAME}/favicon.PNG`,
      copyright: 'All rights reserved, KICAWEB',
      feedLinks: {
        json: `${CONFIG.URL}json/`,
        atom: `${CONFIG.URL}atom/`,
      },
      author: {
        name: 'Noelia Romero',
        email: 'noelia@kicaweb.com',
        link: 'https://www.kicaweb.com/noelia-romero-roy/',
      },
    });

    const filter = {
      webData: {
        $elemMatch: {
          webCode: CONFIG.WEBCODE,
          publishDate: {
            // RSS delivers scholarships 30 min later than the publishDate due AdSense requirements
            $lte: new Date(new Date().getTime() - 30 * 60 * 1000),
          },
        },
      },
    };
    const options = {
      limit: 20,
      sort: { 'webData.0.publishDate': -1 },
    };

    const scholarships = await Scholarship.getScholarships(filter, options);

    scholarships.sort((a, b) => {
      return new Date(b.publishDate) - new Date(a.publishDate);
    });

    scholarships.forEach((scholarship) => {
      feed.addItem({
        title: scholarship.title,
        id: scholarship.id,
        link: `${CONFIG.URL}${scholarship.slug}/`,
        description: scholarship.description,
        category: scholarship.categories[0].name,
        content: scholarship.description,
        author: [
          {
            name: 'Noelia Romero',
            email: 'noelia@kicaweb.com',
            link: 'https://www.kicaweb.com/noelia-romero-roy/',
          },
        ],
        contributor: [
          {
            name: 'Óscar Giménez',
            email: 'oscar@kicaweb.com',
            link: 'https://www.kicaweb.com/oscar-gimenez-aldabas/',
          },
        ],
        date: scholarship.publishDate,
      });
    });

    return feed;
  }
}

module.exports = Rss;
