/* global CONFIG */
const express = require('express');
const app = express();

require('dotenv').config();
global.CONFIG = process.env;

app.use(express.static(`${__dirname}/public`));
app.use(express.static(`${__dirname}/views`));

app.use(require('./server/middlewares/'));
app.use(require('./server/routes/'));

const hbs = require('hbs');
app.set('view engine', 'hbs');
hbs.registerPartials(`${__dirname}/views/partials/`);
require('./views/helpers/');

const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const dbUrl = `mongodb://${CONFIG.DB_USER}:${CONFIG.DB_PASSWORD}@${CONFIG.DB_HOSTNAME}:${CONFIG.DB_PORT}/${
  CONFIG.DB_NAME
}`;

const Logger = require('./server/common/Logger');

mongoose.connect(dbUrl, { useNewUrlParser: true }, (err) => {
  if (err) {
    Logger.dbConnection('FAILURE');
    throw err;
  }
  Logger.dbConnection('OK');
});

app.listen(CONFIG.APP_PORT, () => {
  Logger.init();
});
